--- The following table for reference
--- (e.g. data extracted from the inspire database is cashed here to guarantee fast access)
--
CREATE TABLE PAPER(
   INSPIREID  INTEGER NOT NULL PRIMARY KEY,
   AUTHORS    CHARACTER VARYING(100)[],       -- list of authors
   TITLE      TEXT,                           -- title of the paper
   JOURNAL    CHARACTER VARYING(100),         -- name of the scientific journal
   ERN        CHARACTER VARYING(100),         -- electronic-resource-num
   PAGES      CHARACTER VARYING(50),          -- page numbers
   VOLUME     CHARACTER VARYING(50),          -- journal volume
   YEA        INTEGER,                        -- year of publication
   ABSTR      TEXT,                           -- abstract
   KEYWORDS   CHARACTER VARYING(50)[],        -- keywords
   LINKURL    TEXT                            -- link to the e.g. inspire url for this paper
);

--
--  distionary table defining the ACCESS of a test result
--
CREATE TABLE access (
    id  SERIAL PRIMARY KEY,
    STATUS CHARACTER VARYING(50) NOT NULL
);

INSERT INTO public.access (STATUS) VALUES ('public');                  -- visible to everyone
INSERT INTO public.access (STATUS) VALUES ('internal');                -- visible only to geant 4 caloborators
INSERT INTO public.access (STATUS) VALUES ('temporary');               -- to be deleted

CREATE TABLE HISTOGRAM(
       ID SERIAL PRIMARY KEY,	
       title VARCHAR(500),
       axis_title VARCHAR(100)[],
       value_title VARCHAR(100)[],
       nbins INT[],
       bin_min REAL[],
       bin_max REAL[],
       value REAL[],
       value_err_sys_plus REAL[],
       value_err_sys_minus REAL[],
       value_err_stat_plus REAL[],
       value_err_stat_minus REAL[]
);

CREATE TABLE CHART(
       ID SERIAL PRIMARY KEY,
       title VARCHAR(500),
       npoints INT,
       x_axis_title VARCHAR(100)[],
       x_val REAL[],
       x_err_stat_plus REAL[],
       x_err_stat_minus REAL[],
       x_err_sys_plus REAL[],
       x_err_sys_minus REAL[]   
);

/* 
// Classical example to "serialize" matrix into pseudo-dimentional array
int jimmy [HEIGHT * WIDTH];
int n,m;

int main ()
{
  for (n=0; n<HEIGHT; n++)
    for (m=0; m<WIDTH; m++)
    {
      jimmy[n*WIDTH+m]=(n+1)*(m+1);
    }
}
*/

CREATE TABLE MCTOOL(
   id SERIAL PRIMARY KEY,	
   name VARCHAR(20) NOT NULL,
   version VARCHAR(20) NOT NULL,
   unique (name, version)
);

CREATE TABLE MODEL(
   ID SERIAL PRIMARY KEY,
   NAME  VARCHAR(50) UNIQUE
);

INSERT INTO "public".MODEL(NAME) VALUES ('FTFP_BERT');
INSERT INTO "public".MODEL(NAME) VALUES ('QGSP_BERT');
INSERT INTO "public".MODEL(NAME) VALUES ('FTFP_BERT_HP');

CREATE TABLE PARTICLE(
   PDGID INTEGER PRIMARY KEY,
   NAME  VARCHAR(50) UNIQUE
);

CREATE TABLE MATERIAL(
   ID SERIAL PRIMARY KEY,
   NAME  VARCHAR(50) UNIQUE
);

CREATE TABLE OBSERVABLE(
   ID SERIAL PRIMARY KEY,
   NAME  VARCHAR(50) UNIQUE
);

CREATE TABLE REACTION(
   ID SERIAL PRIMARY KEY,
   NAME  VARCHAR(50) UNIQUE
);

CREATE TABLE BEAM(
   ID SERIAL PRIMARY KEY,
   PARTICLE INTEGER REFERENCES PARTICLE(PDGID),    
   ENERGY REAL                              -- kinetic Energy in MeV
);

--
-- table representing dictionary of geant 4 working group
--
CREATE TABLE wgroups (
    id serial  PRIMARY KEY,              -- working group id
    name      CHARACTER VARYING(50) UNIQUE     -- name of the working group            
);

--
-- table representing a geant 4 test
--
CREATE TABLE TEST (
    id      INTEGER NOT NULL PRIMARY KEY,            -- number matches geant 4 test number if applicable
    name    CHARACTER VARYING(50) UNIQUE,                   -- name of the test
    description text,                                    -- text describing the purpose of the test
    responsible CHARACTER VARYING(100)[],                -- names of persons responsible for the test
    wg          INTEGER REFERENCES wgroups(id),        -- geant 4 working group that the test is associated with
    keywords    VARCHAR(50)[],                           -- keywords associated with the test
    inspirerefs INTEGER[]                                -- array elements reference inspireid in table inspirereference
);

--
-- table representing a EXPERIMENT
--
CREATE TABLE EXPERIMENT(
    ID      SERIAL PRIMARY KEY,                -- number matches EXPERIMENT number if applicable
    NAME    CHARACTER VARYING(50),             -- name of the EXPERIMENT
    DESCRIPTION TEXT,                                    -- TEXT describing the purpose of the EXPERIMENT
    KEYWORDS    VARCHAR(50)[],                           -- keywords associated with the EXPERIMENT
    INSPIREREFS INTEGER[]                                -- array elements reference inspireid in table inspirereference
);

--
--
CREATE TABLE EXPERIMENTDEF (
    id SERIAL PRIMARY KEY,
    EID INTEGER REFERENCES EXPERIMENTS(ID) UNIQUE,
    links INTEGER[]                                         -- LINKS THE EXPERIMENTRESULT(TRID) LISTING THE EXPERIMENTAL RESULTS THAT SHOULD BE PLOTTED BY DEFAULT
);

--
-- table representing a test result
-- it defines the meta data and links to a histogram or xy data set

CREATE TABLE DATATABLE (
   ID       SERIAL  PRIMARY KEY,
   TESTID     INTEGER REFERENCES TEST(ID),                      -- REFERENCES THE TEST THAT TEST RESULT IS ASSOCIATED WITH
   MCTOOL     INTEGER REFERENCES MCTOOL(ID),                           -- REFERENCES TO PROGRAM USED
   MODEL      CHARACTER VARYING(50) REFERENCES MODEL(name),            -- REFERENCES TABLE WITH THE GEANT 4 MODEL OR PHYSICSLIST USED
   BEAM       INTEGER REFERENCES BEAM(ID),                             -- REFERENCES PARTICLE TABLE (PDGID,NAME, ...)
   TARGET     CHARACTER VARYING(50) REFERENCES MATERIAL(name),         -- REFERENCES TABLE defining materials
   OBSERVABLE CHARACTER VARYING(50) REFERENCES OBSERVABLE(name),       -- REFERENCES TABLE defininG OBSERVABLES (CROSS SECTION, MOMENTUM OF OUTGOING PARTICLES..)
   SECONDARY  INTEGER REFERENCES PARTICLE(PDGID),                      -- REFERENCES PARTICLE TABLE (PDGID,NAME, ...)
   REACTION   CHARACTER VARYING(50) REFERENCES REACTION(NAME),         -- REFERENCES REACTION TABLE (SCATTERING, PARTICLE PRODUCTION, CAPTURE, DECAY....)
-- DATA_TABLE INTEGER REFERENCES DATA_TABLE(ID),                       -- REFERENCES 1d HISTOGRAM
   PAPER      INTEGER REFERENCES PAPER(INSPIREID),                            -- REFERENCE to original paper with inspire, id, experiment, authors etc 
   STATUS     INTEGER REFERENCES ACCESS(ID),                           -- controls who has access to the test result
   KEYWORDS   VARCHAR(50)[]                                            -- keywords associated with the testresult e.g. angle of outgoing particle
);

--
-- the following table provides a links to the EXPERIMENTRESULT that should be displayed as  default
--
CREATE TABLE TESTDEF (
    id SERIAL PRIMARY KEY,
    tID INTEGER REFERENCES test(ID) UNIQUE,              -- LINK TO ASSOCIATED TEST
    links INTEGER[]                                           -- LINKS THE EXPERIMENTRESULT(TRID) LISTING THE EXPERIMENTAL RESULTS THAT SHOULD BE PLOTTED BY DEFAULT
);