-- https://hepdata.net/record/ins1220862

/* Table 1

#: name: Table 1
#: description: The measured differential elastic cross section.

#: data_file: Table1.yaml
#: keyword reactions: P P --> P P
#: keyword observables: DSIG/DT
#: keyword cmenergies: 7000.0
#: RE,,P P -->P P
#: SQRT(S),,7000.0
'ABS(T) IN GEV**2 LOW','ABS(T) IN GEV**2 HIGH','D(SIG)/DT IN MB/GEV**2','stat +','stat -','sys +','sys -'
0.0046,0.0057,465.0,27.0,27.0,21.0,21.0
0.0057,0.0073,465.0,11.0,11.0,21.0,21.0
0.0073,0.009059,437.5,5.0,5.0,19.1,19.1
0.009059,0.01084,411.0,3.3,3.3,17.7,17.7
0.01084,0.01263,402.3,2.9,2.9,17.3,17.3
0.01263,0.01445,384.5,2.6,2.6,16.5,16.5

CREATE TABLE HISTOGRAM(
       ID SERIAL PRIMARY KEY,
       title VARCHAR(500),
       axis_title VARCHAR(100)[],
       value_title VARCHAR(100)[],
       nbins INT[],
       bin_min REAL[],
       bin_max REAL[],
       value REAL[],
       value_err_sys_plus REAL[],
       value_err_sys_minus REAL[],
       value_err_stat_plus REAL[],
       value_err_stat_minus REAL[]
);

*/

INSERT INTO HISTOGRAM(title, 
		      axis_title, 
                      value_title, 
                      nbins, 
                      bin_min, 
                      bin_max, 
                      value, 
                      value_err_sys_plus, 
                      value_err_sys_minus, 
                      value_err_stat_plus, 
                      value_err_stat_minus )
VALUES (              'The measured differential elastic cross section',
	              '{"ABS(T) IN GEV**2"}',
                      '{"D(SIG)/DT IN MB/GEV**2"}',
                      '{24}',
                      '{0.0046, 0.0057, 0.0073, 0.009059, 0.01084, 0.01263}',
                      '{0.0057, 0.0073, 0.009059, 0.01084, 0.01263, 0.01445}',
                       '{465.0, 465.0, 437.5, 411.0, 402.3, 384.5}',
                       '{21.0, 21.0, 19.1, 17.7,17.3, 16.5}',
                       '{21.0, 21.0, 19.1, 17.7,17.3, 16.5}',
                       '{27.0, 11.0, 5.0, 3.3, 2.9, 2.6}',
                       '{27.0, 11.0, 5.0, 3.3, 2.9, 2.6}');
                       
              






*/

-- entering 2D histogram 
--        nbins INT[],
--       bin_min REAL[],
--       bin_max REAL[],
--       value REAL[],
-- we have 2 binned axis, so we put vector of min. values of bins for first axis, and then vector of "mins" for second axis

1st axis:
   low  edges '{0.0046, 0.0057, 0.0073, 0.009059, 0.01084, 0.01263}',
   high edges '{0.0057, 0.0073, 0.009059, 0.01084, 0.01263, 0.01445}',

2nd axis:
   low  edges '{ 1,  2 , 3 , 4 }',
   high edges '{ 2,  3 , 4 , 5 }',


So: bin_min and bin_max will be the following:

   bin_min = { 0.0046, 0.0057, 0.0073, 0.009059, 0.01084, 0.01263 , 1,  2 , 3 , 4 }
   bin_max = { 0.0057, 0.0073, 0.009059, 0.01084, 0.01263, 0.01445, 2,  3 , 4 , 5 }


values, we have matrix of 6 x 4 and create pseudo-dimensional histogram and put it into "value" ( clear enough or has to be explained better? )

And of course in order to read it back we need to put dimension of the matrix into "nbins", 

i.e. nbins = {6 , 4 } 

 



  






-- How to enter CHART/SCATTER plot data
-- A scatter plot, scatterplot, or scattergraph is a type of mathematical diagram 
-- using Cartesian coordinates to display values for typically two variables for a set of data. 

-- in short, graph represents points in 2D( 3D etc) space
-- 2D: A(x1, x2), B(x1, x2)
-- 3D: A(x1, x2, x3), B(x1, x2, x3)
-- it is always same amount of enrties in X1, X2, X3... XN axis
-- so it is characterized by number of points.

/*

CREATE TABLE CHART(
       ID SERIAL PRIMARY KEY,
       title VARCHAR(500),
       npoints INT,
       x_axis_title VARCHAR(100)[],
       x_val REAL[],
       x_err_stat_plus REAL[],
       x_err_stat_minus REAL[],
       x_err_sys_plus REAL[],
       x_err_sys_minus REAL[]
);

*/

-- example - 
-
- we set set of point in 3D space with errors
-
-  5 points  
-  X1 coordinates - {0., 1., 2., 3., 4., 5.}
-  X2 coordinated - {100., 101., 102., 103., 104.}  
-  X1 errors stat plus -      { 2., 2., 2., 2., 2.} 
-  X2 errors stat plus -      { 5., 5., 5., 5., 5.}
-  X1 errors stat minus -      { 2., 2., 2., 2., 2.}
-  X2 errors stat minus -      { 5., 5., 5., 5., 5.}

INSERT INTO CHART(title,
                      npoints,
                      x_axis_title,
                      x_val,
                      x_err_stat_plus,
                      x_err_stat_minus)
VALUES (              'example of 2D scatter plot',
	  	      5,
                      '{"name of axis of X1", "name of axis of X2"}',
		      '{0., 1., 2., 3., 4., 5.,   100., 101., 102., 103., 104.}',
		      '{2., 2., 2., 2., 2. ,      5., 5., 5., 5., 5.}',
		      '{2., 2., 2., 2., 2. ,      5., 5., 5., 5., 5.}');







